package com.chatty.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kneeh.awesome.AwesomeDB;
import com.kneeh.awesome.Where;
import com.kneeh.chat.Message;

@WebServlet("/CheckMessage")
public class CheckMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CheckMessage() {
		super();
		// TODO Auto-generated constructor stub
		if (!AwesomeDB.isInitialized())
			AwesomeDB.init("localhost", "chatty");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String user = request.getParameter("user");
		String time = request.getParameter("time");
		// 1479309433535

		try {
			ArrayList<Message> messages = (ArrayList<Message>) AwesomeDB.getInstance().getList("messages",
					Message.class, new Where("added_at", ">", time), new Where("u_to", "=", "\'" + user + "\'"));
			if (messages.isEmpty())
				response.getWriter().append('0');
			else
				response.getWriter().append(new Gson().toJson(messages));
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
