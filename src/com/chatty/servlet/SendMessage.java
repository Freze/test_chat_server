package com.chatty.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kneeh.awesome.AwesomeDB;
import com.kneeh.chat.Message;

@WebServlet("/SendMessage")
public class SendMessage extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SendMessage() {
		super();
		if (!AwesomeDB.isInitialized())
			AwesomeDB.init("localhost", "chatty");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Not Avialable on GET");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String msg = request.getParameter("msg");
		String from = request.getParameter("from");
		String to = request.getParameter("to");
		response.getWriter().append("Msg : " + msg).append("<br>From : " + from).append("<br>To : " + to);
		Message message = new Message();
		message.setFrom(from);
		message.setMsg(msg);
		message.setTo(to);
		message.setDate("17:21");
		message.setAdded_at(System.currentTimeMillis());
		try {
			AwesomeDB db = AwesomeDB.getInstance();
			db.insert("messages", message);
		} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
