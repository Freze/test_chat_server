package com.chatty.servlet;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kneeh.awesome.AwesomeDB;
import com.kneeh.awesome.Where;
import com.kneeh.chat.User;

@WebServlet("/Login")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Login() {
		super();
		if (!AwesomeDB.isInitialized())
			AwesomeDB.init("localhost", "chatty");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		String username = request.getParameter("username");
		String password = request.getParameter("password");
		User user;
		try {
			user = AwesomeDB.getInstance().get("users", User.class, new Where("username", "=", "\'"+username+"\'"),
					new Where("password", "=", "\'"+password+"\'"));
			if (user == null)
				response.getWriter().append('1');
			else
				response.getWriter().append(new Gson().toJson(user));
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			response.getWriter().append('0');
			response.getWriter().append(e.getMessage());
		}

	}

}
