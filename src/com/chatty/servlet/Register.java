package com.chatty.servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.lang.reflect.InvocationTargetException;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.kneeh.awesome.AwesomeDB;
import com.kneeh.awesome.Where;
import com.kneeh.chat.User;

@WebServlet("/Register")
public class Register extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public Register() {
		super();
		if (!AwesomeDB.isInitialized())
			AwesomeDB.init("localhost", "chatty");
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Not avialable in GET");
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		try {
			if (AwesomeDB.getInstance().get("users", User.class) != null) {
				response.getWriter().append('1');
			} else {
				User user = new User();
				user.setName(username);
				user.setPassword(password);
				user.setUsername(username + "@chatty.com");
				AwesomeDB.getInstance().insert("users", user);
				user = AwesomeDB.getInstance().get("users", User.class,
						new Where("username", "=", "\'" + user.getUsername() + "\'"));
				response.getWriter().println(new Gson().toJson(user));
			}
		} catch (NoSuchMethodException | SecurityException | InstantiationException | IllegalAccessException
				| IllegalArgumentException | InvocationTargetException | SQLException e) {
			// TODO Auto-generated catch block
			StringWriter sw = new StringWriter();
			PrintWriter pw = new PrintWriter(sw);
			e.printStackTrace(pw);
			response.getWriter().println(sw.toString());

		}

	}

}
